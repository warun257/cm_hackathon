package com.cm.hackathone;

import java.io.*;
import java.net.*;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONObject;

public class ChannelStatistics {

	private static String channelId = "UC5jPILcMDpMCinKQU7ZFHCg";
	private static List<String> allVideoIds = new ArrayList<String>();
	public JSONArray getAllVideoInfo(JSONObject channelJson) {
		JSONArray commentsJson = new JSONArray();
		channelJson.remove("kind");
		channelJson.remove("etag");
		channelJson.remove("regionCode");
		channelJson.remove("pageInfo");
		JSONArray items = channelJson.getJSONArray("items");
		for (int i = 0; i < items.length(); i++) {
			JSONObject commentSection = items.getJSONObject(i)
					.getJSONObject("snippet").getJSONObject("topLevelComment")
					.getJSONObject("snippet");
			commentSection.remove("updatedAt");
			commentSection.remove("publishedAt");
			commentSection.remove("authorChannelId");
			commentSection.remove("authorProfileImageUrl");
			commentSection.remove("authorChannelUrl");
			commentsJson.put(commentSection);
		}
		return commentsJson;
	}

	public static String httpGet(String urlStr) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		if (conn.getResponseCode() != 200) {
			throw new IOException(conn.getResponseMessage());
		}

		// Buffer the result into a string
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		rd.close();

		conn.disconnect();
		return sb.toString();
	}

	public JSONArray getAllCommentsInfo(String videoId) {
		String comments = null;
		try {
			comments = httpGet("https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&videoId="
					+ videoId + "&key=AIzaSyC7sQyG1tu6A5S0djwBvQvjqiqYcsgLj8U");
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONObject commentsJson = new JSONObject(comments);
		JSONArray jsonOfComments = new JSONArray();
		commentsJson.remove("kind");
		commentsJson.remove("etag");
		commentsJson.remove("regionCode");
		commentsJson.remove("pageInfo");
		JSONArray items = commentsJson.getJSONArray("items");
		for (int i = 0; i < items.length(); i++) {
			JSONObject commentSection = items.getJSONObject(i)
					.getJSONObject("snippet").getJSONObject("topLevelComment")
					.getJSONObject("snippet");
			commentSection.remove("updatedAt");
			commentSection.remove("publishedAt");
			commentSection.remove("authorChannelId");
			commentSection.remove("authorProfileImageUrl");
			commentSection.remove("authorChannelUrl");
			jsonOfComments.put(commentSection);
		}
		return jsonOfComments;
	}

	public JSONObject getVideoCounts(String videoId) {
		String video = null;
		try {
			video = httpGet("https://www.googleapis.com/youtube/v3/videos?id="
					+ videoId
					+ "&part=statistics&key=AIzaSyC7sQyG1tu6A5S0djwBvQvjqiqYcsgLj8U");
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONObject videoJson = new JSONObject(video);
		return videoJson.getJSONArray("items").getJSONObject(0)
				.getJSONObject("statistics");
	}

	public Map<String,Video> getAllVideoIds() {
		Map<String,Video> videoIds= new HashMap<String,Video>();
		String channelPage = null;
		try {
			channelPage = httpGet("https://www.googleapis.com/youtube/v3/search?key=AIzaSyC7sQyG1tu6A5S0djwBvQvjqiqYcsgLj8U&channelId=UC5jPILcMDpMCinKQU7ZFHCg&part=snippet&order=date&maxResults=20");
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONArray channelPageJson = new JSONObject(channelPage)
				.getJSONArray("items");
		for (int i = 0; i < channelPageJson.length(); i++) {
			JSONObject videoSection = channelPageJson.getJSONObject(i)
					.getJSONObject("id");
			if (videoSection.getString("kind").equals("youtube#video")) {
				Video currentVideo = new Video();
				currentVideo.setId(videoSection.getString("videoId"));
				currentVideo.setTitle(channelPageJson.getJSONObject(i).getJSONObject("snippet").getString("title"));
				currentVideo.setDescription(channelPageJson.getJSONObject(i).getJSONObject("snippet").getString("description"));
				videoIds.put(videoSection.getString("videoId"), currentVideo);
				allVideoIds.add(videoSection.getString("videoId"));
			}
		}
		return videoIds;
	}

	public JSONObject getChannelData() {
		ChannelStatistics o1 = new ChannelStatistics();
		JSONObject channelInfo = new JSONObject();
		channelInfo.put("channelId", channelId);
		
		JSONArray videoArray = new JSONArray();
		Map<String, Video> videoIds = o1.getAllVideoIds();
		for (String videoId : allVideoIds) {
			JSONObject currentVideoInfo = new JSONObject();
			Video video = videoIds.get(videoId);
			currentVideoInfo.put("title",video.getTitle());
			currentVideoInfo.put("id",video.getId());
			currentVideoInfo.put("description",video.getDescription());
			currentVideoInfo.put("comments", o1.getAllCommentsInfo(videoId));
			currentVideoInfo.put("video_counts", o1.getVideoCounts(videoId));
			videoArray.put(currentVideoInfo);
		}
		channelInfo.put("videos", videoArray);
		return channelInfo;
	}
}